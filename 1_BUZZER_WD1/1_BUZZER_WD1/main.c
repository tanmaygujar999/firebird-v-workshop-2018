/*
 * 1_BUZZER_WD1.c
 *
 * Created: 1/21/2018 5:27:45 AM
 * Author : TEJESH JADHAV
 */ 
#define F_CPU 14745600L

#include <avr/io.h>
#include <util/delay.h>


int main(void)
{
	DDRC = 0x08;		//Set PORTC  PIN 3 as OUTPUT
    while (1)			//The infinite loop
    {
		PORTC ^= 0x08;	//portc = portc xor 0x08......which means [(0000 1000) xor (0000 1000)]....we only want to toggle the 3rd pin of the port without changing the state of other pins of that port. 
		_delay_ms(1000);
    }
}

