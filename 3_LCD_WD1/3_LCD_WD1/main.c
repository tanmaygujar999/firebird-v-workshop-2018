/*
 * 3_LCD_WD1.c
 *
 * Created: 1/21/2018 3:45:01 AM
 * Author : Tejesh Jadhav
 */ 

/*
 LCD Connections:
				 LCD	  Microcontroller Pins
				
					RS  --> PC0
					RW  --> PC1
					EN  --> PC2
					DB7 --> PC7
					DB6 --> PC6
					DB5 --> PC5
					DB4 --> PC4




*/
#define F_CPU 14745600L

#include <avr/io.h>
#include <util/delay.h>
#include "LCD.h"			//Located in \3_LCD_WD1\3_LCD_WD1\LCD.h


int main(void)
{
	DDRC = 0xF7;			//Set PORTC as output (Excluding pin 3 of PORTC as it has buzzer on it)	
	lcd_set_4bit();			//Set LCD type 4bit or 8bit
	lcd_init();				//Initialize LCD
while(1)					//The infinite loop
    {
		lcd_cursor(1,1);	//Set Cursor of LCD
		lcd_string(" WELCOME TO THE");	//Print String
		lcd_cursor(2,4);
		lcd_string(" WORKSHOP ");
		_delay_ms(2000);
    }
}

