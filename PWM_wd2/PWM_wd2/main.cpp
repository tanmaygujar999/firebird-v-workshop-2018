/*
 * PWM_wd2.cpp
 *
 * Created: 21-01-2018 15:07:40
 * Author : tanma
 */ 

#define F_CPU 14745600L
#include <avr/io.h>
#include <util/delay.h>

void run_motor()
{
	DDRA = 0xFF; //direction port as output
	DDRL = 0xFF; //pwm port as output
	
	PORTA = (1<<PA1) | (1<<PA2); //set direction
	
	TCCR5A = (1<<COM5A1)|(1<<COM5B1)|(1<<WGM50);//set compare output modes for motors
	OCR5A = 255;
	OCR5B = 255;//set motor speeds
	TCCR5B = (1<<WGM52) | (1<<CS50) | (1<<CS52);//set pwm mode
	
}

int main(void)
{
    /* Replace with your application code */
	run_motor();
    while (1) 
    {
		
    }
}

